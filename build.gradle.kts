tasks.register("assemble") {
    group = "build"
    dependsOn(gradle.includedBuild("market-service").task(":assemble"))
}

tasks.register("check") {
    group = "build"
    dependsOn(gradle.includedBuild("market-service").task(":check"))
}

tasks.register("jib") {
    group = "build"
    dependsOn(gradle.includedBuild("market-service").task(":jib"))
}