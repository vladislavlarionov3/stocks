package com.larionov.stocks.services.predictiontester.repository

import com.larionov.stocks.services.predictiontester.domain.predictiontest.PredictionTest
import org.springframework.data.jpa.repository.JpaRepository

interface PredictorsTestRepository : JpaRepository<PredictionTest, Long>