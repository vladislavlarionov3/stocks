package com.larionov.stocks.services.predictiontester.repository

import com.larionov.stocks.services.predictiontester.domain.function.ValueListFunction
import org.springframework.data.jpa.repository.JpaRepository

interface ValueListFunctionRepository : JpaRepository<ValueListFunction, Long>