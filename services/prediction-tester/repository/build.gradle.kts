apply("../../../gradle/templates/repository.gradle")

dependencies {
    implementation(project(":services:prediction-tester:domain:function"))
    implementation(project(":services:prediction-tester:domain:prediction-test"))
}