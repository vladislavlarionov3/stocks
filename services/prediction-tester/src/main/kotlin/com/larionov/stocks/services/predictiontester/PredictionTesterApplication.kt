package com.larionov.stocks.services.predictiontester

import com.larionov.stocks.services.market.api.CandleClient
import com.larionov.stocks.services.predictiontester.domain.gbpredictor.GBPredictorFactory
import com.larionov.stocks.services.predictiontester.domain.gbpredictor.GBSampler
import com.larionov.stocks.services.predictiontester.domain.gbpredictor.GBTrainer
import com.larionov.stocks.services.predictiontester.domain.predictiontest.CurrentValuePredictor
import com.larionov.stocks.services.predictiontester.domain.predictiontest.PredictionTest
import com.larionov.stocks.services.predictiontester.domain.predictiontest.Predictor
import com.larionov.stocks.services.predictiontester.repository.PredictorsTestRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.transaction.support.TransactionTemplate
import java.time.Instant

private const val LOGISTIC_REG = "reg:logistic"

@SpringBootApplication
@EnableConfigurationProperties(PredictionTesterProperties::class)
@EnableFeignClients(clients = [CandleClient::class])
class PredictionTesterApplication {
    @Bean
    fun runner(
        datasetsProvider: DatasetsProvider,
        repository: PredictorsTestRepository,
        predictors: List<Predictor>,
        transactionTemplate: TransactionTemplate
    ) = CommandLineRunner {
        transactionTemplate.execute {
            PredictionTest(
                creationInstant = Instant.now(),
                originalFunction = datasetsProvider.testFunction(),
                rangeFrom = 95.0,
                rangeTo = 98.0,
                step = 0.00005,
                horizon = 0.00005
            )
                .let { repository.save(it) }
                .also { it.run(predictors) }
                .let { repository.save(it) }
        }
    }

    @Bean
    fun currentValuePredictor() = CurrentValuePredictor()

//    @Bean
//    fun gbPredictor1(gbTrainer1: GBTrainer) = GBPredictorFactory(
//        gbTrainer1,
//        0.00005,
//        "fast"
//    )
//        .create()

    @Bean
    fun gbPredictor2(gbTrainer2: GBTrainer) = GBPredictorFactory(
        gbTrainer2,
        0.00005,
        "long"
    )
        .create()

    @Bean
    fun gbPredictor3(gbTrainer3: GBTrainer) = GBPredictorFactory(
        gbTrainer3,
        0.00005,
        "test"
    )
        .create()

    @Bean
    fun gbSampler() = GBSampler(predictionHorizon = 0.00005)

    @Bean
    fun gbTrainer1(datasetsProvider: DatasetsProvider) = GBTrainer(
        xgbParams = mapOf(
            Pair("eta", 0.01),
            Pair("max_depth", 3),
//                            Pair("gamma", 1),
            Pair("min_child_weight", 1),
            Pair("objective", LOGISTIC_REG),
//                            Pair("eval_metric", "logloss"),
            Pair("nthread", Runtime.getRuntime().availableProcessors()),
//                            Pair("num_class", 1),
        ),
        roundNumber = 50,
        gbSampler = gbSampler(),
        trainDataShare = 0.8,
        values = datasetsProvider.train1Function().values
    )

    @Bean
    fun gbTrainer2(datasetsProvider: DatasetsProvider) = GBTrainer(
        xgbParams = mapOf(
            Pair("eta", 0.01),
            Pair("max_depth", 3),
//                            Pair("gamma", 1),
            Pair("min_child_weight", 1),
            Pair("objective", LOGISTIC_REG),
//                            Pair("eval_metric", "logloss"),
            Pair("nthread", Runtime.getRuntime().availableProcessors()),
//                            Pair("num_class", 1),
        ),
        roundNumber = 300,
        gbSampler = gbSampler(),
        trainDataShare = 0.8,
        values = datasetsProvider.train2Function().values
    )

    @Bean
    fun gbTrainer3(datasetsProvider: DatasetsProvider) = GBTrainer(
        xgbParams = mapOf(
            Pair("eta", 0.01),
            Pair("max_depth", 3),
//                            Pair("gamma", 1),
            Pair("min_child_weight", 1),
            Pair("objective", LOGISTIC_REG),
//                            Pair("eval_metric", "logloss"),
            Pair("nthread", Runtime.getRuntime().availableProcessors()),
//                            Pair("num_class", 1),
        ),
        roundNumber = 300,
        gbSampler = gbSampler(),
        trainDataShare = 0.8,
        values = datasetsProvider.testFunction().values
    )

}

fun main(args: Array<String>) {
    runApplication<PredictionTesterApplication>(*args)
}
