package com.larionov.stocks.services.predictiontester

import kotlinx.datetime.Instant
import org.springframework.stereotype.Component

@Component
class TimeMapper {
    fun ofInstant(instant: Instant) =
        100 * (instant.epochSeconds.toDouble() - 1.51671162E9) / (1.64583264E9 - 1.51671162E9)

//    fun toDuration(value: Double) = value.seconds
}