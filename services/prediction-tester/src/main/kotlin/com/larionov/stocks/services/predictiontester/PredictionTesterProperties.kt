package com.larionov.stocks.services.predictiontester

import kotlinx.datetime.LocalDate
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "extrapolation-tester")
data class PredictionTesterProperties(
    val trainInstrumentId: Int = 7723,
    val testInstrumentId: Int = 7723,

    val trainFromDate: LocalDate = LocalDate(2018, 4, 1),
    val trainToDate: LocalDate = LocalDate(2020, 1, 1),

//    val testFromDate: LocalDate = LocalDate(2020, 0, 1),
    val testFromDate: LocalDate = LocalDate(2021, 9, 1),
    val testToDate: LocalDate = LocalDate(2022, 1, 1)

)
