package com.larionov.stocks.services.predictiontester

import com.larionov.stocks.services.market.api.CandleDto
import com.larionov.stocks.services.predictiontester.domain.function.FunctionValue
import kotlinx.datetime.toKotlinInstant
import org.springframework.stereotype.Component

@Component
class CandlesMapper(val timeMapper: TimeMapper) {

    fun map(candle: CandleDto) = FunctionValue(
        x = timeMapper.ofInstant(candle.instant.toKotlinInstant()),
        y = candle.open,
        labels = mutableSetOf("original")
    )

}