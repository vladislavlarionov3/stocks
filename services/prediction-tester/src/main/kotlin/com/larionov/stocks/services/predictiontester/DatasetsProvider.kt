package com.larionov.stocks.services.predictiontester

import com.larionov.stocks.services.market.api.CandleClient
import com.larionov.stocks.services.predictiontester.domain.function.ValueListFunction
import mu.KotlinLogging
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class DatasetsProvider(
    private val client: CandleClient,
    private val mapper: CandlesMapper,
    private val properties: PredictionTesterProperties
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    private lateinit var wholeFunction: ValueListFunction

    @PostConstruct
    fun init() {
        val instrumentId = properties.testInstrumentId
        wholeFunction = client.getSortedByInstant(instrumentId, null, null, null)
            .let {
                log.info { "Candles requested, instrumentId: $instrumentId, size: ${it.size}" }
                it.map(mapper::map)
            }
            .let { ValueListFunction(values = it, labels = mutableSetOf(instrumentId.toString())) }
//            .let { ValueListFunction(values = it.split(0.5).first.values, labels = it.labels) }
    }

    fun train1Function() = wholeFunction.split(0.1).first

    fun train2Function() = wholeFunction.split(0.1).second.split(0.9).first

    fun testFunction() = wholeFunction.split(0.1).second.split(0.9).second

}