package com.larionov.stocks.services.predictiontester.domain.predictiontest

import com.larionov.stocks.services.predictiontester.domain.function.LinearInterpolatedFunction

interface Predictor {
    fun predictValue(function: LinearInterpolatedFunction, currentX: Double): Double

    fun isPredictable(function: LinearInterpolatedFunction, currentX: Double): Boolean

    fun name(): String
}