package com.larionov.stocks.services.predictiontester.domain.predictiontest

import com.larionov.stocks.services.predictiontester.domain.function.FunctionValue
import com.larionov.stocks.services.predictiontester.domain.function.LinearInterpolatedFunction
import com.larionov.stocks.services.predictiontester.domain.function.ValueListFunction
import mu.KotlinLogging
import java.time.Instant
import java.time.Instant.now
import javax.persistence.*

@Entity
class PredictionTest(
    @Id
    @GeneratedValue
    private val id: Long? = null,

    private val creationInstant: Instant,

    @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
    private val originalFunction: ValueListFunction,

    private val horizon: Double,

    private val rangeFrom: Double,
    private val rangeTo: Double,
    private val step: Double,

    @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
    private var result: PredictionTestResult? = null

) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    fun run(predictors: List<Predictor>) = LinearInterpolatedFunction(originalFunction.values)
        .also { log.info { "Test started, predictors ${predictors.size}" } }
        .let { function -> predictors.map { run(it, function) } }
        .let {
            result = PredictionTestResult(
                test = this,
                creationInstant = now(),
                predictedFunctions = it
            )
        }

    private fun run(predictor: Predictor, function: LinearInterpolatedFunction) =
//        generateSequence(rangeFrom) { it + step }.takeWhile { it < rangeTo }
        originalFunction.values
            .filter { predictor.isPredictable(function, it.x - horizon) }
            .map {
                FunctionValue(
                    x = it.x,
                    y = predictor.predictValue(function, it.x - horizon),
                    labels = mutableSetOf(predictor.name())
                )
            }
            .toMutableList()
            .let { ValueListFunction(values = it) }
            .also {
                it.relativeDiff(function).abs().let {
                    log.info {
                        "Predictor: ${predictor.name()}, " +
                                "relative error average: ${it.avg()}, " +
                                "relative error dispersion: ${it.dispersion()}"
                    }
                }
            }


}