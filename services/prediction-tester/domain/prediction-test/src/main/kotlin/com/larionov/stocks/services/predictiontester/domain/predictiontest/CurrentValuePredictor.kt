package com.larionov.stocks.services.predictiontester.domain.predictiontest

import com.larionov.stocks.services.predictiontester.domain.function.LinearInterpolatedFunction

class CurrentValuePredictor : Predictor {

    override fun predictValue(function: LinearInterpolatedFunction, currentX: Double) =
        function.value(currentX)

    override fun isPredictable(function: LinearInterpolatedFunction, currentX: Double) =
        function.isValid(currentX)

    override fun name() = "current-value"

}