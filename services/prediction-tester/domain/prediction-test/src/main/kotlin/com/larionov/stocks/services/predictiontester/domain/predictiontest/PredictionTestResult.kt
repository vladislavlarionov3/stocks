package com.larionov.stocks.services.predictiontester.domain.predictiontest

import com.larionov.stocks.services.predictiontester.domain.function.ValueListFunction
import java.time.Instant
import javax.persistence.*

@Entity
class PredictionTestResult(
    @Id
    private val testId: Long? = null,

    @OneToOne
    @MapsId
    private val test: PredictionTest,

    val creationInstant: Instant,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    val predictedFunctions: List<ValueListFunction>
)