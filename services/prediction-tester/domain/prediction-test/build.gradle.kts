plugins {
    id("org.jetbrains.kotlin.plugin.jpa")
}
apply("../../../../gradle/templates/jpa.gradle")

dependencies {
    implementation(project(":services:prediction-tester:domain:function"))
}