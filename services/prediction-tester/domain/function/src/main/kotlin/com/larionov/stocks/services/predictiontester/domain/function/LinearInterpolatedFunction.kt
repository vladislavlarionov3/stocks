package com.larionov.stocks.services.predictiontester.domain.function

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator

class LinearInterpolatedFunction(values: List<FunctionValue>) {
    private val interpolatedFunction = values.let {
        LinearInterpolator().interpolate(
            it.map(FunctionValue::x).toDoubleArray(),
            it.map(FunctionValue::y).toDoubleArray()
        )
    }

    fun value(x: Double) = interpolatedFunction.value(x)

    fun isValid(x: Double) = interpolatedFunction.isValidPoint(x)

}