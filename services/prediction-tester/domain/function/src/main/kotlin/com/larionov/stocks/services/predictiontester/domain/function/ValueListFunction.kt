package com.larionov.stocks.services.predictiontester.domain.function

import javax.persistence.*
import kotlin.math.absoluteValue

@Entity
class ValueListFunction(
    @Id
    @GeneratedValue
    val id: Long? = null,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    val values: List<FunctionValue> = mutableListOf(),

    @ElementCollection
    val labels: Set<String> = mutableSetOf()

) {
    fun split(share: Double) = split((share * values.size).toInt())

    private fun split(index: Int) = Pair(
        ValueListFunction(values = values.subList(0, index)),
        ValueListFunction(values = values.subList(index, values.size))
    )

    fun relativeDiff(function: LinearInterpolatedFunction) = values
        .map { it.relativeDiff(function.value(it.x)) }
        .let { ValueListFunction(values = it) }

    fun abs() = values
        .map { FunctionValue(x = it.x, y = it.y.absoluteValue, labels = setOf()) }
        .let { ValueListFunction(values = it) }

    fun avg() = values.map { it.y }.average()

    fun dispersion() = avg()
        .let { avg -> values.map { (avg - it.y).absoluteValue } }
        .average()

}