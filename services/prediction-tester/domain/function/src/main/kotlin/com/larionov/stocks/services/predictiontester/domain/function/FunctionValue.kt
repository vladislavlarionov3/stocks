package com.larionov.stocks.services.predictiontester.domain.function

import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class FunctionValue(
    @Id
    @GeneratedValue
    val id: Long? = null,

    val x: Double,
    val y: Double,

    @ElementCollection
    val labels: Set<String>

) {
    fun relativeDiff(otherY: Double) = FunctionValue(x = x, y = (y - otherY) / otherY, labels = setOf())
}