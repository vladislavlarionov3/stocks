plugins {
    id("org.jetbrains.kotlin.plugin.jpa")
}
apply("../../../../gradle/templates/jpa.gradle")

dependencies {
    implementation("org.apache.commons:commons-math3:3.6.1")
}