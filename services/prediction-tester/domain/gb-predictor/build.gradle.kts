dependencies {
    implementation(project(":services:prediction-tester:domain:function"))
    implementation(project(":services:prediction-tester:domain:prediction-test"))

    implementation(files("xgboost4j_2.12-1.6.0.jar"))
}
