package com.larionov.stocks.services.predictiontester.domain.gbpredictor

import com.larionov.stocks.services.predictiontester.domain.function.LinearInterpolatedFunction
import com.larionov.stocks.services.predictiontester.domain.predictiontest.Predictor
import ml.dmlc.xgboost4j.java.Booster
import ml.dmlc.xgboost4j.java.DMatrix

class GBPredictor(
    private val booster: Booster,
    private val horizon: Double,
    private val label: String
) : Predictor {

    override fun predictValue(function: LinearInterpolatedFunction, currentX: Double) =
        ArithmeticProgressionSumsSequence.of(5, horizon, 0.0)
            .map { currentX - it }
            .map { function.value(it) }
            .let { GBSampleArgs(it) }
            .let { predictValue(it) }

    override fun isPredictable(function: LinearInterpolatedFunction, currentX: Double) =
        ArithmeticProgressionSumsSequence.of(5, horizon, 0.0)
            .map { currentX - it }
            .all { function.isValid(it) }

    private fun predictValue(args: GBSampleArgs) = args.normalize()
        .toFloatArray()
        .let { DMatrix(it, 1, it.size, 0.0f) }
        .let { booster.predict(it) }
        .let { it[0][0] }
        .toDouble()
        .let { args.denormalize(it) }

    override fun name() = "gb-$horizon-$label"

}