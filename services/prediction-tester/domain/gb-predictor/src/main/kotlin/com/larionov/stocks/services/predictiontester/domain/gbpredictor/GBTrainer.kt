package com.larionov.stocks.services.predictiontester.domain.gbpredictor

import com.larionov.stocks.services.predictiontester.domain.function.FunctionValue
import ml.dmlc.xgboost4j.java.Booster
import ml.dmlc.xgboost4j.java.DMatrix
import ml.dmlc.xgboost4j.java.XGBoost
import mu.KotlinLogging
import java.io.File

class GBTrainer(
    private val values: List<FunctionValue>,
    private val xgbParams: Map<String, Any>,
    private val roundNumber: Int,
    private val modelPath: String = "out/model.bin",
    private val trainDataShare: Double,
    private val gbSampler: GBSampler
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    val booster: Booster by lazy {
        (trainDataShare * values.size).toInt()
            .let {
                log.info { "GB training started, train samples: $it, test samples: ${values.size - it}" }
                listOf(
                    values.subList(0, it),
                    values.subList(it, values.size),
                )
            }
            .map { gbSampler.create(it) }
            .map { createMatrix(it) }
            .let {
                XGBoost.train(
                    it.first(),
                    xgbParams,
                    roundNumber,
                    mapOf(
                        Pair("train", it.first()),
                        Pair("test", it.last())
                    ),
                    null,
                    null
                )
            }.also {
                File(modelPath).run {
                    parentFile.mkdirs()
                    it.saveModel(outputStream())
                }
            }
    }

    private fun createMatrix(samples: List<GBSample>) =
        samples.fold(mutableListOf<Float>()) { samplesAsFloatList, sample ->
            samplesAsFloatList.addAll(sample.args.toFloatList())
            samplesAsFloatList
        }
            .toFloatArray()
            .let { DMatrix(it, samples.size, it.size / samples.size, 0.0f) }
            .apply { label = samples.map { it.result.toFloat() }.toFloatArray() }

}