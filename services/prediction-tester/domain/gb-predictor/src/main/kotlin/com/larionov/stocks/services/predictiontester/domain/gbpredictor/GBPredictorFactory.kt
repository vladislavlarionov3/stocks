package com.larionov.stocks.services.predictiontester.domain.gbpredictor

class GBPredictorFactory(
    private val gbTrainer: GBTrainer,
    private val predictionHorizon: Double,
    private val label: String = ""
) {
    fun create() = GBPredictor(
        gbTrainer.booster,
        predictionHorizon,
        label
    )
}