package com.larionov.stocks.services.predictiontester.domain.gbpredictor

import com.larionov.stocks.services.predictiontester.domain.function.FunctionValue
import com.larionov.stocks.services.predictiontester.domain.function.LinearInterpolatedFunction

class GBSampler(
    private val argsSize: Int = 5,
    private val progressionIncrement: Double = 0.0,
    private val predictionHorizon: Double
) {
    fun create(values: List<FunctionValue>) = create(values, LinearInterpolatedFunction(values))

    private fun create(values: List<FunctionValue>, function: LinearInterpolatedFunction) =
        values.map { create(it, function) }

    private fun create(value: FunctionValue, function: LinearInterpolatedFunction) =
        ArithmeticProgressionSumsSequence.of(argsSize, predictionHorizon, progressionIncrement)
            .map { value.x - predictionHorizon - it }
            .map { function.value(it) }
            .let { GBSampleArgs(it) }
            .let { GBSample(it, value.y) }
            .normalize()
}