package com.larionov.stocks.services.predictiontester.domain.gbpredictor

class ArithmeticProgressionSumsSequence {
    companion object {
        fun of(size: Int, first: Double = 0.0, increment: Double) = (0 until size)
            .map { (2 * first + increment * (it - 1)) * (it) / 2 }
    }
}