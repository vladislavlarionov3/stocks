package com.larionov.stocks.services.predictiontester.domain.gbpredictor

data class GBSampleArgs(private val args: List<Double>) {
    private val normalizationFactor = 2 * args.last()

    fun normalize() = GBSampleArgs(args.map { normalize(it) })

    fun normalize(value: Double) = value / normalizationFactor

    fun denormalize(result: Double) = normalizationFactor * result

    fun toFloatArray() = toFloatList().toFloatArray()

    fun toFloatList() = args.map { it.toFloat() }
}