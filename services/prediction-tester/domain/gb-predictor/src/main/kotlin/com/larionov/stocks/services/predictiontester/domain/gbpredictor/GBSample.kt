package com.larionov.stocks.services.predictiontester.domain.gbpredictor

data class GBSample(
    val args: GBSampleArgs,
    val result: Double
) {
    fun normalize() = GBSample(args.normalize(), args.normalize(result))

}