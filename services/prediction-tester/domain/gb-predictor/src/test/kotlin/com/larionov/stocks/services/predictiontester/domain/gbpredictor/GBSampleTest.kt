package com.larionov.stocks.services.predictiontester.domain.gbpredictor

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GBSampleTest {

    @Test
    fun normalize() {
        GBSampleArgs(listOf(1.0, 2.0, 4.0))
            .let {
                assertEquals(
                    GBSample(it, 12.0).normalize(),
                    GBSample(it.normalize(), 1.5)
                )
            }

    }
}