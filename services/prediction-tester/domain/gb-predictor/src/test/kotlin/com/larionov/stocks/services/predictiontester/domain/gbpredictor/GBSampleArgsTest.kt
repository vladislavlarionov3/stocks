package com.larionov.stocks.services.predictiontester.domain.gbpredictor

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GBSampleArgsTest {

    private val args = GBSampleArgs(listOf(1.0, 2.0, 4.0))

    @Test
    fun normalize() {
        assertEquals(
            GBSampleArgs(listOf(0.125, 0.25, 0.5)),
            args.normalize()
        )
    }

    @Test
    fun normalizeValue() {
        assertEquals(
            3.0,
            args.normalize(24.0)
        )
    }

    @Test
    fun denormalize() {
        assertEquals(
            12.0,
            args.denormalize(1.5)
        )
    }

}