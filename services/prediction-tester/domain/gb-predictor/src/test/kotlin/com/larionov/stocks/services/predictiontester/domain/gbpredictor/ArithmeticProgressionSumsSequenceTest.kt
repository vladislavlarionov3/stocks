package com.larionov.stocks.services.predictiontester.domain.gbpredictor

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ArithmeticProgressionSumsSequenceTest {
    @Test
    fun whenEmpty() {
        assertEquals(
            listOf<Double>(),
            ArithmeticProgressionSumsSequence.of(0, 0.0, 0.0)
        )
    }

    @Test
    fun whenSingleElement() {
        assertEquals(
            listOf(0.0),
            ArithmeticProgressionSumsSequence.of(1, 1.0, 1.0)
        )
    }

    @Test
    fun whenStepIsZero() {
        assertEquals(
            listOf(0.0, 0.0),
            ArithmeticProgressionSumsSequence.of(2, 0.0, 0.0)
        )
    }

    @Test
    fun whenIncrementIsZero() {
        assertEquals(
            listOf(0.0, 1.0, 2.0),
            ArithmeticProgressionSumsSequence.of(3, 1.0, 0.0)
        )
    }

    @Test
    fun whenIncremented() {
        assertEquals(
            listOf(0.0, 1.0, 3.0, 6.0, 10.0),
            ArithmeticProgressionSumsSequence.of(5, 1.0, 1.0)
        )
    }

}