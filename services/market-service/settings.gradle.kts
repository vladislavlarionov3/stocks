includeBuild("controller") {
    dependencySubstitution {
        substitute(module("com.larionov.stocks.market:controller")).using(project(":"))
    }
}