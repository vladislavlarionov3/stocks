package com.larionov.stocks.market.api

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient("market-service", path = MarketInstrumentClient.URL, contextId = MarketInstrumentClient.URL)
interface MarketInstrumentClient {
    companion object {
        const val URL = "market-instruments"
    }

    @GetMapping("{id}/figi")
    fun getFigi(@PathVariable id: Int): String

    @GetMapping("exists", params = ["figi"])
    fun exists(@RequestParam figi: String): Boolean

    @PostMapping
    fun post(@RequestBody instruments: List<MarketInstrumentDto>): List<MarketInstrumentDto>

}