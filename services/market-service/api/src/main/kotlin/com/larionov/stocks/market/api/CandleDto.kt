package com.larionov.stocks.market.api

import java.time.Instant

data class CandleDto(
    val id: Long?,
    val instrumentId: Int,

    val instant: Instant,

    val open: Double,
    val close: Double,

    val high: Double,
    val low: Double,

    val volume: Int,

    val intervalInMillis: Long
)