package com.larionov.stocks.market.api

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import java.time.Instant

@FeignClient("market-service", path = CandleClient.URL, contextId = CandleClient.URL)
interface CandleClient {
    companion object {
        const val URL = "candles"
    }

    @GetMapping(params = ["instrumentId", "fromInstant", "toInstant"])
    fun get(
        @RequestParam instrumentId: Int,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) fromInstant: Instant,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) toInstant: Instant
    ): List<CandleDto>

    @GetMapping(params = ["instrumentId"])
    fun getSortedByInstant(
        @RequestParam instrumentId: Int,
        @RequestParam pageNumber: Int?,
        @RequestParam pageSize: Int?,
        @RequestParam desc: Boolean?
    ): List<CandleDto>

    @PostMapping
    fun post(@RequestBody candles: List<CandleDto>): List<CandleDto>
}