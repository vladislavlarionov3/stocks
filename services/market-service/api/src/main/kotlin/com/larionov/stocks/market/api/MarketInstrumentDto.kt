package com.larionov.stocks.market.api

data class MarketInstrumentDto(
    val id: Int?,

    val figi: String,
    val name: String,

    val type: String,
    val currency: String,
    val lot: Int,
    val minPriceIncrement: Double?
)