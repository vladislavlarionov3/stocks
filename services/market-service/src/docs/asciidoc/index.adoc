= Api docs
:toc: left
:source-highlighter: highlightjs
:doctype: book

= Market api examples
:defaultSnippets: snippets='http-request,http-response'

:operation: When generated and get all then result is not empty array
== {operation}

operation::{operation}[{defaultSnippets}]

:operation: When generated and get fifi then return generated

== {operation}

operation::{operation}[{defaultSnippets}]

:operation: When generated then exists by figi

== {operation}

operation::{operation}[{defaultSnippets}]

:operation: When post then exists in repository

== {operation}

operation::{operation}[{defaultSnippets}]

:operation: When post without required parameter then status is bad request

== {operation}

operation::{operation}[{defaultSnippets}]

include::../../../build/generate-resources/main/index.adoc[]