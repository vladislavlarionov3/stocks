includeBuild("../api") {
    dependencySubstitution {
        substitute(module("com.larionov.stocks.market:api")).using(project(":"))
    }
}
includeBuild("../repository") {
    dependencySubstitution {
        substitute(module("com.larionov.stocks.market:repository")).using(project(":"))
    }
}