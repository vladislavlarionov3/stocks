package com.larionov.stocks.market

import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcBuilderCustomizer
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration
import org.springframework.restdocs.operation.preprocess.Preprocessors.*

@TestConfiguration
class MockMvcConfiguration {
    @Bean
    fun restDocumentationMockMvcBuilderCustomizer(restDocumentation: RestDocumentationContextProvider) =
        MockMvcBuilderCustomizer { builder ->
            documentationConfiguration(restDocumentation)
                .run(builder::apply)
                .alwaysDo(
                    document(
                        "{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                    )
                )
        }
}