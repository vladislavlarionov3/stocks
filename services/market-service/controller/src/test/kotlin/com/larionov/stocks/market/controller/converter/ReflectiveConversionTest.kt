package com.larionov.stocks.market.controller.converter

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class ReflectiveConversionTest {

    @Test
    fun whenSourcePropertyNotNullable_ThenTargetEqual() {
        class Source(val value: Int)
        class Target(val value: Int)

        val value = 1
        val target = convert(Source(value), Source::class, Target::class)

        assertEquals(value, target.value)
    }

    @Test
    fun whenSourcePropertyIsNull_ThenTargetIsNull() {
        class Source(val value: Int?)
        class Target(val value: Int?)

        val target = convert(Source(null), Source::class, Target::class)

        assertNull(target.value)
    }

    @Test
    fun whenCustomProperty_ThenTargetEqual() {
        class Source
        class Target(val value: Int)

        val value = 1

        val target = convert(
            Source(), Source::class, Target::class,
            Pair(Target::value, value)
        )

        assertEquals(value, target.value)
    }

    @Test
    fun whenPropertyNotFound_ThenException() {
        class Source
        class Target(val value: Int)

        assertThrows(IllegalArgumentException::class.java) {
            convert(Source(), Source::class, Target::class)
        }
    }

}