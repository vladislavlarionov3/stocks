package com.larionov.stocks.market

import org.springframework.test.context.DynamicPropertyRegistry
import org.testcontainers.containers.PostgreSQLContainer

fun postgres(): PostgreSQLContainer<*> = PostgreSQLContainer("postgres:15")
    .withDatabaseName("market_service")

fun addPostgresProperties(
    postgres: PostgreSQLContainer<*>,
    registry: DynamicPropertyRegistry
) {
    registry.add("spring.datasource.url", postgres::getJdbcUrl)
    registry.add("spring.datasource.password", postgres::getPassword)
    registry.add("spring.datasource.username", postgres::getUsername)
}