package com.larionov.stocks.market

import com.larionov.stocks.market.controller.MarketInstrumentController
import com.larionov.stocks.market.controller.MarketInstrumentGenerator
import com.larionov.stocks.market.controller.converter.MarketInstrumentConverter
import com.larionov.stocks.market.repository.MarketInstrumentRepository
import kotlinx.serialization.json.buildJsonArray
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers


@ExtendWith(
    SpringExtension::class,
    RestDocumentationExtension::class
)
@SpringBootTest(
    classes = [
        MarketInstrumentController::class,
        MarketInstrumentConverter::class,
        MarketInstrumentRepository::class,
        MarketInstrumentGenerator::class,
        MockMvcConfiguration::class
    ],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@EnableAutoConfiguration
@AutoConfigureMockMvc
@AutoConfigureRestDocs
@Testcontainers
class MarketInstrumentControllerIT {
    @Autowired
    lateinit var generator: MarketInstrumentGenerator

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var repository: MarketInstrumentRepository

    @Test
    fun `When generated and get all then result is not empty array`() {
        generator.generateAndSave()
        mockMvc.perform(get(URL))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$").isArray)
            .andExpect(jsonPath("$").isNotEmpty)
    }

    @Test
    fun `When generated and get fifi then return generated`() {
        generator.generateAndSave()
            .run {
                mockMvc.perform(get("${URL}/{id}/figi", id))
                    .andExpect(status().isOk)
                    .andExpect(content().string(figi))
            }
    }

    @Test
    fun `When generated then exists by figi`() {
        generator.generateAndSave()
            .run {
                get("${URL}/exists")
                    .param("figi", figi)
            }
            .let(mockMvc::perform)
            .andExpect(status().isOk)
            .andExpect(content().string(true.toString()))
    }

    @Test
    fun `When post then exists in repository`() {
        generator.generate()
            .apply {
                buildJsonArray {
                    add(buildJsonObject {
                        put("figi", figi)
                        put("name", name)
                        put("type", type)
                        put("currency", currency)
                        put("lot", 0.1)
                    })
                }
                    .toString()
                    .let {
                        post(URL)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(it)
                    }
                    .let(mockMvc::perform)
                    .andExpect(status().isOk)
            }
            .run { Assertions.assertTrue(repository.existsByFigi(figi)) }
    }

    @Test
    fun `When post without required parameter then status is bad request`() {
        generator.generate()
            .apply {
                buildJsonArray {
                    add(buildJsonObject {
                        put("figi", figi)
                        put("name", name)
                        put("type", type)
                        put("currency", currency)
                    })
                }
                    .toString()
                    .let {
                        post(URL)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(it)
                    }
                    .let(mockMvc::perform)
                    .andExpect(status().isBadRequest)
            }
    }

    companion object {
        private const val URL = "/market-instruments"

        @Container
        private val postgres = postgres()

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) =
            addPostgresProperties(postgres, registry)
    }

}