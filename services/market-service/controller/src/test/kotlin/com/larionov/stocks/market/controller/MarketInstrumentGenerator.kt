package com.larionov.stocks.market.controller

import com.larionov.stocks.market.domain.MarketInstrument
import com.larionov.stocks.market.repository.MarketInstrumentRepository
import org.springframework.boot.test.context.TestComponent
import java.util.concurrent.atomic.AtomicInteger

@TestComponent
class MarketInstrumentGenerator(val repository: MarketInstrumentRepository) {

    private val nextId = AtomicInteger()

    fun generateAndSave(): MarketInstrument = generate().let(repository::save)

    fun generate() = nextId.getAndIncrement()
        .let {
            MarketInstrument(
                id = it,
                figi = "figi-$it",
                name = "Some stock #$it",
                currency = "USD",
                type = "STOCK",
                lot = 1,
                minPriceIncrement = 0.1
            )
        }


}