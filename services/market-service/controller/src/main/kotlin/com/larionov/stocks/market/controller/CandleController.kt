package com.larionov.stocks.market.controller

import com.larionov.stocks.market.api.CandleClient
import com.larionov.stocks.market.api.CandleDto
import com.larionov.stocks.market.controller.converter.CandleConverter
import com.larionov.stocks.market.repository.CandleRepository
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
@RequestMapping(CandleClient.URL)
@Transactional(readOnly = true)
class CandleController(
    val repository: CandleRepository,
    val converter: CandleConverter
) : CandleClient {
    override fun get(instrumentId: Int, fromInstant: Instant, toInstant: Instant) =
        repository.findAllByInstrumentIdAndInstantBetween(instrumentId, fromInstant, toInstant)
            .map(converter::convert)

    override fun getSortedByInstant(
        instrumentId: Int,
        pageNumber: Int?,
        pageSize: Int?,
        desc: Boolean?
    ) =
        repository.findAllByInstrumentId(
            instrumentId,
            PageRequest.of(
                pageNumber ?: 0,
                pageSize ?: Int.MAX_VALUE,
                if (desc == true) Sort.Direction.DESC else Sort.Direction.ASC,
                "instant"
            )
        ).map(converter::convert)

    @Transactional
    override fun post(candles: List<CandleDto>) =
        candles.map(converter::convert)
            .run(repository::saveAll)
            .map(converter::convert)
}