package com.larionov.stocks.market.controller.converter

import com.larionov.stocks.market.api.CandleDto
import com.larionov.stocks.market.domain.Candle
import com.larionov.stocks.market.repository.MarketInstrumentRepository
import org.springframework.stereotype.Component

@Component
class CandleConverter(private val instrumentRepository: MarketInstrumentRepository) {
    fun convert(candle: Candle) = convert(
        candle, Candle::class, CandleDto::class,
        CandleDto::instrumentId to candle.instrument.id
    )

    fun convert(dto: CandleDto) = convert(
        dto, CandleDto::class, Candle::class,
        Candle::instrument to instrumentRepository.getReferenceById(dto.instrumentId)
    )

}