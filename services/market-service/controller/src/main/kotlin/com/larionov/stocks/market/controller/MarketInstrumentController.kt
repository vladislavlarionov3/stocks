package com.larionov.stocks.market.controller

import com.larionov.stocks.market.api.MarketInstrumentClient
import com.larionov.stocks.market.api.MarketInstrumentDto
import com.larionov.stocks.market.controller.converter.MarketInstrumentConverter
import com.larionov.stocks.market.repository.MarketInstrumentRepository
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(MarketInstrumentClient.URL)
@Transactional(readOnly = true)
class MarketInstrumentController(
    val repository: MarketInstrumentRepository,
    val converter: MarketInstrumentConverter
) : MarketInstrumentClient {
    @GetMapping
    fun getAll() = repository.findAll().map(converter::convert)

    override fun getFigi(id: Int) = repository.getReferenceById(id).figi

    override fun exists(figi: String) = repository.existsByFigi(figi)

    @Transactional
    override fun post(instruments: List<MarketInstrumentDto>) =
        instruments.map(converter::convert)
            .run(repository::saveAll)
            .map(converter::convert)

}