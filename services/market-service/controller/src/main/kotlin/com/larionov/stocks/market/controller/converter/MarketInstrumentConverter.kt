package com.larionov.stocks.market.controller.converter

import com.larionov.stocks.market.api.MarketInstrumentDto
import com.larionov.stocks.market.domain.MarketInstrument
import org.springframework.stereotype.Component

@Component
class MarketInstrumentConverter {
    fun convert(dto: MarketInstrumentDto) =
        convert(dto, MarketInstrumentDto::class, MarketInstrument::class)

    fun convert(instrument: MarketInstrument) =
        convert(instrument, MarketInstrument::class, MarketInstrumentDto::class)

}