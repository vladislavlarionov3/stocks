package com.larionov.stocks.market.controller.converter

import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

inline fun <reified S : Any, reified T : Any> convert(
    source: S,
    sourceClass: KClass<S>,
    targetClass: KClass<T>,
    vararg customProperties: Pair<KProperty1<T, *>, Any?>
): T = convert(source, sourceClass, targetClass,
    customProperties.associateBy({ it.first.name }) { it.second }
)

inline fun <reified S : Any, reified T : Any> convert(
    source: S,
    sourceClass: KClass<S>,
    targetClass: KClass<T>,
    customPropertyByName: Map<String, Any?>,
): T = sourceClass.memberProperties
    .associateBy({ it.name }) { it.get(source) }
    .let { sourcePropertyByName ->
        { targetPropertyName: String ->
            if (customPropertyByName.containsKey(targetPropertyName))
                customPropertyByName[targetPropertyName]
            else if (sourcePropertyByName.containsKey(targetPropertyName))
                sourcePropertyByName[targetPropertyName]
            else throw IllegalArgumentException("Can't convert property $targetPropertyName")
        }
    }
    .let { valueSelector ->
        targetClass.primaryConstructor!!
            .let { targetConstructor ->
                targetConstructor.parameters
                    .associateWith { valueSelector(it.name!!) }
                    .let { targetConstructor.callBy(it) }
            }
    }