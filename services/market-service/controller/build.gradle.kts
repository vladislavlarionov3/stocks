plugins {
    `java-library`
    kotlin("jvm") version "1.6.21"
    id("io.spring.dependency-management") version "1.0.15.RELEASE"
    kotlin("plugin.spring") version "1.6.21"
    id("org.springdoc.openapi-gradle-plugin") version "1.4.0"
    id("org.openapi.generator") version "6.2.1"
    jacoco
}

repositories {
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.boot:spring-boot-dependencies:2.7.5")
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:2021.0.1")
    }
}

dependencies {
    implementation("com.larionov.stocks.market:api")
    implementation("com.larionov.stocks.market:domain")
    implementation("com.larionov.stocks.market:repository")

//    implementation("io.github.microutils:kotlin-logging-jvm:3.0.4")
//    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
    implementation(kotlin("reflect"))

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springdoc:springdoc-openapi-ui:1.6.14")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.14.0")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.restdocs:spring-restdocs-mockmvc")
    testImplementation("org.springframework.boot:spring-boot-starter-data-jpa")
    testImplementation("org.testcontainers:testcontainers:1.17.6")
    testImplementation("org.testcontainers:junit-jupiter:1.17.6")
    testImplementation("org.testcontainers:postgresql:1.17.6")
    testImplementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

openApi {
    customBootRun {
        args.set(listOf("--spring.profiles.active=generate-open-api-docs"))
    }
}

openApiGenerate {
    generatorName.set("asciidoc")
    inputSpec.set("$rootDir/build/openapi.json")
}