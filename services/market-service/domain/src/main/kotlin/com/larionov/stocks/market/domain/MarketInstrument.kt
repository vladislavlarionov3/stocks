package com.larionov.stocks.market.domain

import javax.persistence.*

@Entity
@Table(indexes = [Index(columnList = "figi", unique = true)])
class MarketInstrument(
    @Id
    @GeneratedValue
    val id: Int?,

    val figi: String,
    val name: String,

    val type: String,
    val currency: String,

    val lot: Int,
    val minPriceIncrement: Double?
)