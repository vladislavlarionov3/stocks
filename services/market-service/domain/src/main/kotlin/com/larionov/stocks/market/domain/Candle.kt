package com.larionov.stocks.market.domain


import java.time.Instant
import javax.persistence.*

@Entity
@Table(
    indexes = [
        Index(columnList = "instrument_id,instant", unique = true),
    ]
)
class Candle(
    @Id
    @GeneratedValue
    val id: Long?,

    @ManyToOne
    val instrument: MarketInstrument,

    val instant: Instant,
    val intervalInMillis: Long,

    val open: Double,
    val close: Double,

    val high: Double,
    val low: Double,

    val volume: Int
)