plugins {
    `java-library`
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.jpa") version "1.6.20-RC"
    id("io.spring.dependency-management") version "1.0.15.RELEASE"
}

repositories {
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.boot:spring-boot-dependencies:2.7.5")
    }
}

dependencies {
    implementation("jakarta.persistence:jakarta.persistence-api")
}