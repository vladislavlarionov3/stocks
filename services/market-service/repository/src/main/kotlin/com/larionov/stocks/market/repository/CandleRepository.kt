package com.larionov.stocks.market.repository

import com.larionov.stocks.market.domain.Candle
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import java.time.Instant

interface CandleRepository : JpaRepository<Candle, Long> {
    fun findAllByInstrumentIdAndInstantBetween(
        instrumentId: Int,
        fromInstant: Instant,
        toInstant: Instant
    ): List<Candle>

    fun findAllByInstrumentId(
        instrumentId: Int,
        pageable: Pageable
    ): List<Candle>
}