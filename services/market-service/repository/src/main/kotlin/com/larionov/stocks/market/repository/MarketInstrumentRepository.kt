package com.larionov.stocks.market.repository

import com.larionov.stocks.market.domain.MarketInstrument
import org.springframework.data.jpa.repository.JpaRepository

interface MarketInstrumentRepository : JpaRepository<MarketInstrument, Int> {
    fun existsByFigi(figi: String): Boolean
}