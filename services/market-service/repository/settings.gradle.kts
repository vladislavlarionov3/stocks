includeBuild("../domain") {
    dependencySubstitution {
        substitute(module("com.larionov.stocks.market:domain")).using(project(":"))
    }
}