package com.larionov.stocks.services.tradingstrategytester.domain.trading

import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.atStartOfDayIn

data class Interval(val start: Instant, val finish: Instant) {
    companion object {
        fun of(start: LocalDate, finish: LocalDate, timeZone: TimeZone) =
            Interval(start.atStartOfDayIn(timeZone), finish.atStartOfDayIn(timeZone))
    }

    fun contains(instant: Instant) = start <= instant && instant < finish

}