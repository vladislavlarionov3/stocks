package com.larionov.stocks.services.tradingstrategytester.domain.trading

import kotlinx.datetime.Instant
import java.math.BigDecimal


data class MutableTrading(
    override val startingBalance: Balance,
    override val operations: MutableList<Operation> = mutableListOf(),
    override var finalBalance: Balance = operations.fold(startingBalance, Balance::plus),
    override val tax: BigDecimal
) : Trading {
    fun addOperation(volume: BigDecimal, price: StockPrice) {
        add(Operation(volume, price.value, price.instant, tax))
    }

    fun add(operation: Operation) {
        operations.add(operation)
        finalBalance += operation
    }

    fun sellAll(price: BigDecimal, instant: Instant) {
        add(Operation(-finalBalance.stocks, price, instant, tax))
    }

}