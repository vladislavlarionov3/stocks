package com.larionov.stocks.services.tradingstrategytester.domain.trading

import kotlinx.datetime.Instant
import java.math.BigDecimal
import kotlin.time.Duration

interface Trading {
    val startingBalance: Balance
    val operations: List<Operation>
    val finalBalance: Balance
    val tax: BigDecimal

    fun profitability() = Profitability(startingBalance.cash, finalBalance.cash - startingBalance.cash, duration())

    fun lastOperation() = operations.lastOrNull()

    fun durationFromLastOperation(toInstant: Instant): Duration =
        lastOperation()?.let { toInstant - it.instant } ?: Duration.INFINITE

    fun duration() = operations.last().instant - operations.first().instant
}