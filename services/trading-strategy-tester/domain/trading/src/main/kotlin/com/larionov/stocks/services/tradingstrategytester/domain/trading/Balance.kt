package com.larionov.stocks.services.tradingstrategytester.domain.trading

import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

data class Balance(
    val cash: BigDecimal = BigDecimal.ZERO,
    val stocks: BigDecimal = BigDecimal.ZERO
) {
    operator fun plus(operation: Operation) = Balance(
        (cash - operation.amount() - operation.taxAmount()).round(MathContext.DECIMAL32),
        (stocks + operation.volume).round(MathContext.DECIMAL32)
    )

    operator fun plus(operations: List<Operation>): Balance = operations.fold(this, Balance::plus)

    fun cacheToStocks(price: BigDecimal, tax: BigDecimal) =
        (BigDecimal.ONE - tax) * cacheToStocks(price)

    private fun cacheToStocks(price: BigDecimal) = cash.divide(price, MathContext.DECIMAL32)

    fun round() = Balance(cash.setScale(2, RoundingMode.HALF_UP), stocks.setScale(2, RoundingMode.HALF_UP))

}
