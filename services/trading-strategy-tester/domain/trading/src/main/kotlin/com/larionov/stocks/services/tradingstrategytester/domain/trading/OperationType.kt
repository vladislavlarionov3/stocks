package com.larionov.stocks.services.tradingstrategytester.domain.trading

enum class OperationType {
    BUY, SELL
}