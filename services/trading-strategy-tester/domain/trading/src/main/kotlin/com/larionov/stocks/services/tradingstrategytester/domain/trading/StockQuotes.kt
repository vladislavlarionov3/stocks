package com.larionov.stocks.services.tradingstrategytester.domain.trading

import kotlinx.datetime.Instant
import java.math.BigDecimal
import java.util.*
import kotlin.time.Duration

data class StockQuotes(
    private val name: String,
    private val prices: List<StockPrice>
) {
    fun quotesForAllInstants() = (1 until prices.size)
        .map { StockQuotes(name, prices.subList(0, it)) }

    fun lastPrice() = prices.last()

    fun lastPriceValue() = lastPrice().value

    fun lastPriceValueBeforeEnd(duration: Duration): BigDecimal? =
        trimTail(duration)?.lastPriceValue()

    fun lastPrices(size: Int, initialStep: Duration, stepIncrement: Duration): List<StockPrice>? =
        if (size == 1)
            listOf(lastPrice())
        else
            trimTail(initialStep)
                ?.lastPrices(size - 1, initialStep.plus(stepIncrement), stepIncrement)
                ?.let { it + lastPrice() }

    fun beginInstant() = prices.first().instant

    fun endInstant() = prices.last().instant

    fun duration() = endInstant() - beginInstant()


    fun trim(interval: Interval) = StockQuotes(name, prices.subList(
        prices.indexOfFirst { it.instant >= interval.start },
        prices.indexOfLast { it.instant < interval.finish }
    ))


    fun split(share: Double) = split((share * prices.size).toInt())

    fun size() = prices.size

    private fun split(index: Int) = Pair(
        StockQuotes(name, (prices.subList(0, index))),
        StockQuotes(name, (prices.subList(index, prices.size)))
    )


    fun trimTail(duration: Duration): StockQuotes? =
        pricesBeforeInclusive(endInstant().minus(duration))?.let { StockQuotes(name, it) }

    private fun pricesBeforeInclusive(instant: Instant) =
        lastIndexBeforeInclusive(instant)?.let { prices.subList(0, it + 1) }

    private fun lastIndexBeforeInclusive(instant: Instant) =
        (prices.lastIndex downTo 0).find { prices[it].instant <= instant }

    fun pricesMap() = prices.map { it.instant to it.value }.toMap(TreeMap())
}