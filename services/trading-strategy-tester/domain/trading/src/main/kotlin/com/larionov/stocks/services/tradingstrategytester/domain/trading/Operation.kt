package com.larionov.stocks.services.tradingstrategytester.domain.trading

import kotlinx.datetime.Instant
import java.math.BigDecimal

data class Operation(
    val volume: BigDecimal,
    val price: BigDecimal,
    val instant: Instant,
    val tax: BigDecimal
) {
    val type: OperationType = if (volume.toDouble() > 0) OperationType.BUY else OperationType.SELL

    fun amount(): BigDecimal = volume * price

    fun taxAmount() = tax * amount().abs()

    operator fun invoke(balance: Balance) = balance + this

    operator fun unaryMinus() = Operation(-volume, price, instant, tax)

}

operator fun List<Operation>.invoke(balance: Balance) = balance + this