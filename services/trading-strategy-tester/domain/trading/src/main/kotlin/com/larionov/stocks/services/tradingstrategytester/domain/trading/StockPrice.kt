package com.larionov.stocks.services.tradingstrategytester.domain.trading

import kotlinx.datetime.Instant
import java.math.BigDecimal

data class StockPrice(
    val value: BigDecimal,
    val instant: Instant,
    val volume: Int
)