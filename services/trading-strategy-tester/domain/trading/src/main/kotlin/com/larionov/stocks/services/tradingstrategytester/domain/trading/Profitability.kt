package com.larionov.stocks.services.tradingstrategytester.domain.trading

import java.math.BigDecimal
import java.math.BigDecimal.ONE
import kotlin.math.pow
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days


data class Profitability(
    val startValue: BigDecimal,
    val amount: BigDecimal,
    val period: Duration
) {
    val annualRate = getEquivalentRate(365.days)

    fun getEquivalentRate(period: Duration): Double =
        (ONE + amount / startValue).toDouble().pow(period.div(this.period)) - 1

}