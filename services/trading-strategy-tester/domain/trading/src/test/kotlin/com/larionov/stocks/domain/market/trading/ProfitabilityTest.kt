package com.larionov.stocks.domain.market.trading

import com.larionov.stocks.services.tradingstrategytester.domain.trading.Profitability
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.Duration
import kotlin.math.pow
import kotlin.time.toKotlinDuration

class ProfitabilityTest {

    @Test
    fun test0() {
        val profitability =
            Profitability(1.0.toBigDecimal(), 0.5.toBigDecimal(), Duration.ofDays(365).toKotlinDuration())
        assertEquals(0.5, profitability.annualRate)
    }

    @Test
    fun test1() {
        val profitability =
            Profitability(1.0.toBigDecimal(), 1.0.toBigDecimal(), Duration.ofDays(365).toKotlinDuration())
        assertEquals(1.0, profitability.annualRate)
    }

    @Test
    fun test2() {
        val profitability =
            Profitability(
                1.0.toBigDecimal(),
                3.0.toBigDecimal(),
                Duration.ofDays(365).multipliedBy(2).toKotlinDuration()
            )
        assertEquals(1.0, profitability.annualRate)
    }

    @Test
    fun test3() {
        val profitability =
            Profitability(1.0.toBigDecimal(), (-0.5).toBigDecimal(), Duration.ofDays(365).toKotlinDuration())
        assertEquals(-0.5, profitability.annualRate)
    }

    @Test
    fun test4() {
        val profitability =
            Profitability(1.0.toBigDecimal(), 29.0.toBigDecimal(), Duration.ofDays(365).toKotlinDuration())
        assertEquals(0.32253515909723585, profitability.getEquivalentRate(Duration.ofDays(30).toKotlinDuration()))
    }

    @Test
    fun test5() {
        val profitability =
            Profitability(1.0.toBigDecimal(), 0.1.toBigDecimal(), Duration.ofDays(30).toKotlinDuration())
        assertEquals(1.1.pow(6.0) - 1, profitability.getEquivalentRate(Duration.ofDays(6 * 30).toKotlinDuration()))
    }
}
