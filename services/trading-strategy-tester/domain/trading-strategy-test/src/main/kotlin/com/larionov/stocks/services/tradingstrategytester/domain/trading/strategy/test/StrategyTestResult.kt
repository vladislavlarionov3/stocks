package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.test

import javax.persistence.*

@Entity
class StrategyTestResult(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @ManyToOne
    var testResult: TestResult? = null,

//    val instrumentId: Int,

    val strategyName: String,

    val finalBalance: Double,
    val annualRate: Double,

    val buyOperationsNumber: Int,
    val sellOperationsNumber: Int

)