package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.test

import com.larionov.stocks.services.tradingstrategytester.domain.trading.*
import com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.TradeStrategy
import mu.KotlinLogging
import java.math.BigDecimal
import java.time.Instant

class TradeStrategiesTest(
    private val testInstrumentIds: List<Int>,
    private val tax: BigDecimal = BigDecimal.ZERO,
    private val startingBalance: Balance = Balance(BigDecimal.ONE),
    private val testInterval: Interval,
    private val strategies: List<TradeStrategy>,
    quotesProvider: (Int) -> StockQuotes,
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val testQuotes by lazy {
        testInstrumentIds
            .map { quotesProvider.invoke(it) }
            .map { it.trim(testInterval) }
    }

    fun run() = TestResult(
        instant = Instant.now(),
        strategyResults = strategies.flatMap { run(it) },
        startingBalance = startingBalance.cash.toDouble()
    )

    private fun run(strategy: TradeStrategy): List<StrategyTestResult> {
        log.info { "Test strategy: ${strategy::class.simpleName}" }

        return testQuotes.map { run(it, strategy) }
    }

    private fun run(testQuotes: StockQuotes, strategy: TradeStrategy) = run {
        log.info { "Test quotes ${testQuotes.size()}, interval: [${testQuotes.beginInstant()}, ${testQuotes.endInstant()}]" }

        log.info { "Trading started, balance: $startingBalance" }
        testQuotes.quotesForAllInstants()
            .fold(MutableTrading(startingBalance, tax = tax)) { trading, stockQuotes ->
                strategy.getVolumeToBuy(trading, stockQuotes)
                    .let { trading.addOperation(it, stockQuotes.lastPrice()) }
                trading
            }
    }.let {
        it.sellAll(testQuotes.lastPriceValue(), testQuotes.endInstant())
        log.info { "Trading finished" }

        log.info { "Final balance: ${it.finalBalance.round()}" }
        log.info { "Annual rate: %.2f".format(it.profitability().annualRate) }
        with(
            StrategyTestResult(
                strategyName = strategy.javaClass.simpleName,
                finalBalance = it.finalBalance.round().cash.toDouble(),
                annualRate = it.profitability().annualRate,
                buyOperationsNumber = it.operations.filter { it.type == OperationType.BUY }.size,
                sellOperationsNumber = it.operations.filter { it.type == OperationType.SELL }.size
            )
        ) {
            log.info { "$this" }
            this
        }
    }

}