package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.test

import java.time.Instant
import javax.persistence.*

@Entity
class TestResult(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    val instant: Instant,

    val startingBalance: Double,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "testResult")
    val strategyResults: List<StrategyTestResult>

) {
    init {
        strategyResults.forEach { it.testResult = this }
    }
}