plugins {
    id("org.jetbrains.kotlin.plugin.jpa")
}
apply("../../../../gradle/templates/jpa.gradle")

dependencies {
    implementation(project(":services:trading-strategy-tester:domain:trading-strategy"))
    implementation(project(":services:trading-strategy-tester:domain:trading"))
}