package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import java.math.BigDecimal
import kotlin.time.Duration

class TradeLimiter(
    private val minOperationStep: Duration
) {
    fun getVolumeToBuy(
        trading: Trading,
        quotes: StockQuotes,
        originalStrategy: () -> BigDecimal
    ): BigDecimal = if (trading.durationFromLastOperation(quotes.endInstant()) < minOperationStep)
        BigDecimal.ZERO
    else
        originalStrategy.invoke()
}