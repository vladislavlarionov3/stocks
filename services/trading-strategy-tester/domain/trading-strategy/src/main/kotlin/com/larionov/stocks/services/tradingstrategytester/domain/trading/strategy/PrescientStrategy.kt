package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import kotlinx.datetime.Instant
import java.math.BigDecimal
import java.util.*
import kotlin.time.Duration

class PrescientStrategy(
    private val step: Duration,
    allQuotes: StockQuotes,
) : TradeStrategy {
    private val allPricesMap: TreeMap<Instant, BigDecimal> = allQuotes.pricesMap()

    override fun getVolumeToBuy(trading: Trading, quotes: StockQuotes): BigDecimal =
        allPricesMap.higherEntry(quotes.lastPrice().instant + step)
            ?.value
            ?.let { getBuyableVolume(trading, quotes.lastPriceValue(), it) }
            ?: BigDecimal.ZERO

    private fun getBuyableVolume(trading: Trading, lastPrice: BigDecimal, futurePrice: BigDecimal) = when {
        futurePrice > lastPrice -> trading.finalBalance.cacheToStocks(lastPrice, trading.tax)
        futurePrice < lastPrice -> -trading.finalBalance.stocks
        else -> BigDecimal.ZERO
    }

}