package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import kotlin.time.Duration

class LimitedStrategy(
    private val original: TradeStrategy,
    minOperationStep: Duration,
) : TradeStrategy {
    private val tradeLimiter = TradeLimiter(minOperationStep)

    override fun getVolumeToBuy(trading: Trading, quotes: StockQuotes) =
        tradeLimiter.getVolumeToBuy(trading, quotes) {
            original.getVolumeToBuy(trading, quotes)
        }

}