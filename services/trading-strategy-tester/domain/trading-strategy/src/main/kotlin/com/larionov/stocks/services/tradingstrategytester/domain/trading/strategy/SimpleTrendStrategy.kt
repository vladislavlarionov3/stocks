package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import java.math.BigDecimal
import kotlin.time.Duration

class SimpleTrendStrategy(
    private val step: Duration
) : TradeStrategy {
    override fun getVolumeToBuy(trading: Trading, quotes: StockQuotes): BigDecimal =
        quotes.lastPriceValueBeforeEnd(step)
            ?.let { getVolumeToBuy(trading, quotes.lastPriceValue(), it) }
            ?: BigDecimal.ZERO

    private fun getVolumeToBuy(trading: Trading, lastPrice: BigDecimal, lastPriceBeforeEnd: BigDecimal) = when {
        lastPriceBeforeEnd < lastPrice -> trading.finalBalance.cacheToStocks(lastPrice, trading.tax)
        lastPriceBeforeEnd > lastPrice -> -trading.finalBalance.stocks
        else -> BigDecimal.ZERO
    }
}