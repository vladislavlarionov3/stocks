package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import java.math.BigDecimal

interface TradeStrategy {
    fun getVolumeToBuy(trading: Trading, quotes: StockQuotes): BigDecimal

}