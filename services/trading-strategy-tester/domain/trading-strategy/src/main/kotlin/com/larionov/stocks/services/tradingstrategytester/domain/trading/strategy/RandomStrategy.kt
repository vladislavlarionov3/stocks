package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import kotlin.random.Random

class RandomStrategy : TradeStrategy {

    override fun getVolumeToBuy(trading: Trading, quotes: StockQuotes) =
        if (Random.nextBoolean())
            trading.finalBalance.cacheToStocks(quotes.lastPrice().value, trading.tax)
        else
            -trading.finalBalance.stocks

}