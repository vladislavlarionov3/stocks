package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import java.math.BigDecimal

class InvestStrategy : TradeStrategy {

    override fun getVolumeToBuy(trading: Trading, quotes: StockQuotes): BigDecimal =
        if (trading.operations.isEmpty())
            trading.finalBalance.cacheToStocks(quotes.lastPrice().value, trading.tax)
        else
            BigDecimal.ZERO

}