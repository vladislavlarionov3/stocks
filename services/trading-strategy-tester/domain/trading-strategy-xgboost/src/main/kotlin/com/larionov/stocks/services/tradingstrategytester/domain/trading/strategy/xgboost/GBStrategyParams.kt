package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import kotlin.time.Duration

data class GBStrategyParams(
    val xgbParams: Map<String, Any>,
    val roundNumber: Int,
    val minOperationStep: Duration,
    val predictionPeriod: Duration,
    val argsSize: Int,
    val argsInitialStep: Duration,
    val argsStepIncrement: Duration,
    val trainDataShare: Double = 0.5,
    val modelPath: String = "out/model.bin",
    val maxArgsPeriod: Duration = Duration.INFINITE,
)