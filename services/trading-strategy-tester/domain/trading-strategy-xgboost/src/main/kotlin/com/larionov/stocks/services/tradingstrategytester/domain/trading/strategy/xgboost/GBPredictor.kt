package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import ml.dmlc.xgboost4j.java.Booster
import ml.dmlc.xgboost4j.java.DMatrix

class GBPredictor(var booster: Booster) {
    fun predictPrice(args: GBSampleArgs) = args.normalize().toFloatArray()
        .let { DMatrix(it, 1, it.size, 0.0f) }
        .let { booster.predict(it) }
        .let { args.denormalize(it[0][0]) }

}