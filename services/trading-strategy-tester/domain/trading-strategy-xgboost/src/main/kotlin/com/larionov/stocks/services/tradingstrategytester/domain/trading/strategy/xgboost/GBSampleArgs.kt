package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockPrice
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import java.math.BigDecimal

data class GBSampleArgs(
    val prices: List<StockPrice>
) {

    fun normalize(): List<Float> =
        values().map { normalize(it) } +
                prices.last().instant.toLocalDateTime(TimeZone.UTC).hour.toFloat()

    fun normalize(price: BigDecimal) = (price / (2.toBigDecimal() * maxPrice)).toFloat()

    fun denormalize(value: Float) = 2.toBigDecimal() * maxPrice * value.toBigDecimal()

    private val maxPrice = values().maxOf { it }

    private fun values() = prices.map { it.value }

}