package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockPrice

data class GBSample(val args: GBSampleArgs, val result: StockPrice) {
    fun normalizeResult() = args.normalize(result.value)

}