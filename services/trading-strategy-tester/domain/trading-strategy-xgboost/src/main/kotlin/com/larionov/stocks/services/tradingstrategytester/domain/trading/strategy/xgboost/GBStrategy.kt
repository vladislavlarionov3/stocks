package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import com.larionov.stocks.services.tradingstrategytester.domain.trading.Balance
import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Trading
import com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.TradeStrategy
import java.math.BigDecimal
import java.math.MathContext

class GBStrategy(
    private val sampleArgsMapper: GBSampleArgsMapper,
    private var predictor: GBPredictor? = null
) : TradeStrategy {
    override fun getVolumeToBuy(trading: Trading, quotes: StockQuotes): BigDecimal =
        sampleArgsMapper.map(quotes)
            ?.let { predictor!!.predictPrice(it) }
            ?.let {
                getVolumeToBuy(
                    trading.tax,
                    trading.finalBalance,
                    quotes.lastPriceValue(),
                    it
                )
            }
            ?: BigDecimal.ZERO


    private fun getVolumeToBuy(
        tax: BigDecimal,
        finalBalance: Balance,
        currentPrice: BigDecimal,
        predictedPrice: BigDecimal
    ) = when {
        predictedPrice > currentPrice -> finalBalance.cacheToStocks(currentPrice, tax)
        (currentPrice - predictedPrice).divide(
            currentPrice,
            MathContext.DECIMAL32
        ) > tax -> finalBalance.stocks

        else -> BigDecimal.ZERO
    }

}