package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import kotlin.time.Duration

class GBSampleMapper(
    private val predictionPeriod: Duration,
    private val argsMapper: GBSampleArgsMapper
) {
    fun mapSamples(quotes: StockQuotes) = quotes.quotesForAllInstants()
        .mapNotNull { map(it) }
        .also { println(it) }

    private fun map(quotes: StockQuotes) = quotes.trimTail(predictionPeriod)
        ?.let { argsMapper.map(it) }
        ?.let { GBSample(it, quotes.lastPrice()) }

}