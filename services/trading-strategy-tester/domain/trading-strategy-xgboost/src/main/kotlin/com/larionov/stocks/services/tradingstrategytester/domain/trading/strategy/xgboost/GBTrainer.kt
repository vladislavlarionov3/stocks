package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import ml.dmlc.xgboost4j.java.Booster
import ml.dmlc.xgboost4j.java.DMatrix
import ml.dmlc.xgboost4j.java.XGBoost
import mu.KotlinLogging
import java.io.File
import kotlin.time.DurationUnit
import kotlin.time.toDuration
import kotlin.time.toJavaDuration

class GBTrainer(
    private val params: GBStrategyParams,
    private val sampleMapper: GBSampleMapper
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    fun train(quotesList: List<StockQuotes>): Booster =
        train(createTrainMatrix(quotesList), createTestMatrix(quotesList))

    private fun createTrainMatrix(quotesList: List<StockQuotes>) = quotesList
        .map { it.split(params.trainDataShare).first }
        .flatMap { sampleMapper.mapSamples(it) }
        .also { log.info { "Train samples ${it.size}" } }
        .let { createMatrix(it) }

    private fun createTestMatrix(quotesList: List<StockQuotes>) = quotesList
        .map { it.split(params.trainDataShare).second }
        .flatMap { sampleMapper.mapSamples(it) }
        .also { log.info { "Test samples ${it.size}" } }
        .let { createMatrix(it) }

    private fun train(trainMatrix: DMatrix, testMatrix: DMatrix) = XGBoost.train(
        trainMatrix,
        params.xgbParams,
        params.roundNumber,
        mapOf(Pair("train", trainMatrix), Pair("test", testMatrix)),
        null,
        null
    ).also { saveModelToFile(it) }

    private fun createMatrix(samples: List<GBSample>) =
        samples.fold(mutableListOf<Float>()) { result, case ->
            result.addAll(case.args.normalize())
            result
        }
            .toFloatArray()
            .let {
                createMatrix(
                    it,
                    samples.map(GBSample::normalizeResult).toFloatArray(),
                    samples.size
                )
            }

    private fun createMatrix(data: FloatArray, label: FloatArray, samplesSize: Int) =
        DMatrix(data, samplesSize, data.size / samplesSize, 0.0f)
            .apply { this.label = label }

    private fun println(samples: List<GBSample>) {
        samples.map { it.args }
            .map { it.prices.last().instant - it.prices.first().instant }
            .map { it.toJavaDuration().toMillis() }
            .average()
            .apply { println(toDuration(DurationUnit.MILLISECONDS)) }
    }

    private fun saveModelToFile(it: Booster) {
        with(File(params.modelPath)) {
            parentFile.mkdirs()
            it.saveModel(outputStream())
        }
    }
}
