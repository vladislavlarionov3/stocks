package com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.xgboost

import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes

class GBSampleArgsMapper(private val params: GBStrategyParams) {
    fun map(quotes: StockQuotes) =
        quotes.lastPrices(params.argsSize, params.argsInitialStep, params.argsStepIncrement)
            ?.takeIf { it.last().instant - it.first().instant <= params.maxArgsPeriod }
            ?.let { GBSampleArgs(it) }
}