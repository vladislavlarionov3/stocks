dependencies {
    implementation(project(":services:trading-strategy-tester:domain:trading"))
    implementation(project(":services:trading-strategy-tester:domain:trading-strategy"))

    implementation(files("xgboost4j_2.12-1.6.0.jar"))
}


