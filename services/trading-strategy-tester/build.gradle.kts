import org.springframework.boot.gradle.tasks.bundling.BootJar

apply("../../gradle/templates/service.gradle")

subprojects {
    val bootJar: BootJar by tasks
    bootJar.enabled = false
}

dependencies {
    implementation(project(":services:trading-strategy-tester:domain:trading"))
    implementation(project(":services:trading-strategy-tester:domain:trading-strategy"))
    implementation(project(":services:trading-strategy-tester:domain:trading-strategy-xgboost"))
    implementation(project(":services:trading-strategy-tester:domain:trading-strategy-test"))
    implementation(project(":services:trading-strategy-tester:repository"))

    implementation(project(":services:market-service:api"))

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
    implementation("org.springframework.cloud:spring-cloud-starter-loadbalancer")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    implementation("com.github.ben-manes.caffeine:caffeine")
}