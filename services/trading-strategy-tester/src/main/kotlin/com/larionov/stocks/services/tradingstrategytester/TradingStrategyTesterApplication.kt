package com.larionov.stocks.services.tradingstrategytester

import com.larionov.stocks.services.market.api.CandleClient
import com.larionov.stocks.services.tradingstrategytester.domain.trading.Interval
import com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.*
import com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.test.TradeStrategiesTest
import com.larionov.stocks.services.tradingstrategytester.repository.TestResultRepository
import kotlinx.datetime.TimeZone
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import kotlin.time.DurationUnit
import kotlin.time.toDuration

@SpringBootApplication
@EnableConfigurationProperties(TradingStrategyTesterProperties::class)
@EnableFeignClients(clients = [CandleClient::class])
class TradeStrategyTesterApplication {
    @Bean
    fun test(
        properties: TradingStrategyTesterProperties,
        quotesProvider: QuotesProvider,
        strategies: List<TradeStrategy>
    ) = TradeStrategiesTest(
        properties.testInstrumentIds,
        testInterval = Interval.of(
            properties.testFromDate,
            properties.testToDate,
            TimeZone.UTC
        ),
        tax = properties.tax,
        quotesProvider = quotesProvider::get,
        strategies = listOf(
            InvestStrategy(),
            RandomStrategy(),
            RandomStrategy(),
            SimpleTrendStrategy(1.toDuration(DurationUnit.MINUTES)),
            CounterSimpleTrendStrategy(1.toDuration(DurationUnit.MINUTES)),
            PrescientStrategy(
                step = 1.toDuration(DurationUnit.MINUTES),
                allQuotes = quotesProvider.get(properties.testInstrumentIds.last())
            ),
//            GBStrategy(
//                gbSampleArgsMapper,
//                GBPredictor()
//                GBStrategyParams(
//                    xgbParams = mapOf(
//                        Pair("eta", 0.01),
//                        Pair("max_depth", 3),
////                            Pair("gamma", 1),
//                        Pair("min_child_weight", 1),
//                        Pair("objective", "reg:logistic"),
////                            Pair("eval_metric", "logloss"),
//                        Pair("nthread", Runtime.getRuntime().availableProcessors()),
////                            Pair("num_class", 1),
//                    ),
//                    roundNumber = 40,
//                    trainDataShare = 0.8,
//                    minOperationStep = 1.toDuration(DurationUnit.MINUTES),
//                    predictionPeriod = 1.toDuration(DurationUnit.MINUTES),
//                    argsSize = 6,
//                    argsInitialStep = 1.toDuration(DurationUnit.MINUTES),
//                    argsStepIncrement = 0.01.toDuration(DurationUnit.MINUTES),
////                        maxArgsPeriod = 1.toDuration(DurationUnit.DAYS),
//                )
//            )
        )
    )

    @Bean
    fun runner(
        test: TradeStrategiesTest,
        repository: TestResultRepository
    ) = CommandLineRunner {
        repository.save(test.run())
    }
}

fun main(args: Array<String>) {
    runApplication<TradeStrategyTesterApplication>(*args)
}
