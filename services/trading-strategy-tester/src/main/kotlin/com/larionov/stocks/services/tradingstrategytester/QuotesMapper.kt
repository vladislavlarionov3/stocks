package com.larionov.stocks.services.tradingstrategytester

import com.larionov.stocks.services.market.api.CandleDto
import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockPrice
import kotlinx.datetime.toKotlinInstant
import org.springframework.stereotype.Component

@Component
class QuotesMapper {
    fun map(candle: CandleDto) = StockPrice(
        value = candle.open.toBigDecimal(),
        instant = candle.instant.toKotlinInstant(),
        volume = candle.volume
    )
}