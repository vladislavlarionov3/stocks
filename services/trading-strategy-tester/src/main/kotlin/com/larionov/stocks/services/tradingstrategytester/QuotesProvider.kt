package com.larionov.stocks.services.tradingstrategytester

import com.larionov.stocks.services.market.api.CandleClient
import com.larionov.stocks.services.tradingstrategytester.domain.trading.StockQuotes
import mu.KotlinLogging
import org.springframework.stereotype.Component

@Component
class QuotesProvider(
    private val client: CandleClient,
    private val mapper: QuotesMapper
) {
    companion object {
        private val log = KotlinLogging.logger {}
    }

    fun get(instrumentId: Int) = with(client.getSortedByInstant(instrumentId, null, null, null)) {
        log.info { "Candles requested, instrumentId: $instrumentId, size: $size" }
        StockQuotes("" + instrumentId, map(mapper::map))
    }

}