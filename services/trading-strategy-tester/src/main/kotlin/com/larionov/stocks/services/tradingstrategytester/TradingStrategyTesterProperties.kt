package com.larionov.stocks.services.tradingstrategytester

import kotlinx.datetime.LocalDate
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.math.BigDecimal

@ConstructorBinding
@ConfigurationProperties(prefix = "trading-strategy-tester")
data class TradingStrategyTesterProperties(
    val trainInstrumentIds: List<Int> = listOf(7723),
    val testInstrumentIds: List<Int> = listOf(7723),

    val trainFromDate: LocalDate = LocalDate(2018, 4, 1),
    val trainToDate: LocalDate = LocalDate(2020, 1, 1),

    val testFromDate: LocalDate = LocalDate(2020, 1, 1),
    val testToDate: LocalDate = LocalDate(2022, 1, 1),

    val tax: BigDecimal = BigDecimal.ZERO,
//    val tax: BigDecimal = BigDecimal.valueOf(0.00025),
)
