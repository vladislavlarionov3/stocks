package com.larionov.stocks.services.tradingstrategytester.repository

import com.larionov.stocks.services.tradingstrategytester.domain.trading.strategy.test.TestResult
import org.springframework.data.jpa.repository.JpaRepository

interface TestResultRepository : JpaRepository<TestResult, Long>