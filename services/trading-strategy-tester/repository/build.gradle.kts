apply("../../../gradle/templates/repository.gradle")

dependencies {
    implementation(project(":services:trading-strategy-tester:domain:trading-strategy-test"))
}