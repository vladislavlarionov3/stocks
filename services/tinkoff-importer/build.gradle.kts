apply("../../gradle/templates/service.gradle")

dependencies {
    implementation(project(":services:market-service:api"))

    implementation("com.google.guava:guava:31.0.1-jre")

    implementation("com.squareup.okhttp3:okhttp:4.9.3")
    implementation("ru.tinkoff.invest:openapi-java-sdk-java8:0.5.1")

    implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
    implementation("org.springframework.cloud:spring-cloud-starter-loadbalancer")
    implementation("com.github.ben-manes.caffeine:caffeine")
}