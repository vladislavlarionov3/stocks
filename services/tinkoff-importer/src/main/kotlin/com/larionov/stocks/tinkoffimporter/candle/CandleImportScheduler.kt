package com.larionov.stocks.tinkoffimporter.candle

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class CandleImportScheduler(
    private val candleImporter: CandleImporter
) {
    @Scheduled(fixedRate = 24 * 60 * 60 * 1000L)
    fun import() {
        candleImporter.import()
    }
}