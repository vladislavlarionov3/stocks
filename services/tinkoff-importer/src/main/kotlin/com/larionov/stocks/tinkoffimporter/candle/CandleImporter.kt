package com.larionov.stocks.tinkoffimporter.candle

import com.larionov.stocks.services.market.api.CandleClient
import com.larionov.stocks.services.market.api.MarketInstrumentClient
import com.larionov.stocks.tinkoffimporter.OpenApiRateLimiter
import com.larionov.stocks.tinkoffimporter.TinkoffImporterProperties
import kotlinx.datetime.*
import mu.KotlinLogging
import org.springframework.stereotype.Component
import ru.tinkoff.invest.openapi.model.rest.Candle
import ru.tinkoff.invest.openapi.model.rest.CandleResolution
import ru.tinkoff.invest.openapi.model.rest.Candles
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

@Component
class CandleImporter(
    private val openApiRateLimiter: OpenApiRateLimiter,
    private val candleClient: CandleClient,
    private val marketInstrumentClient: MarketInstrumentClient,
    private val properties: TinkoffImporterProperties,
    private val mapper: CandleMapper
) {
    companion object {
        val CANDLE_RESOLUTION: CandleResolution = CandleResolution._1MIN
        val DATE_ZONE: ZoneOffset = ZoneOffset.UTC
    }

    private val log = KotlinLogging.logger {}

    fun import() {
        log.info { "Import started" }

        properties.instrumentIdsForCandlesImport
            .forEach { importOrLogError(it) }

        log.info { "Import finished" }
    }

    private fun importOrLogError(instrumentId: Int) {
        try {
            import(instrumentId)
        } catch (e: Exception) {
            log.error(e) { "Import error, instrumentId: $instrumentId" }
        }
    }

    private fun import(instrumentId: Int) {
        val fromDate = candleClient.getSortedByInstant(instrumentId, 0, 1, true)
            .map { it.instant.atOffset(DATE_ZONE).toLocalDate().toKotlinLocalDate() }
            .map { it.plus(DateTimeUnit.DAY) }
            .getOrElse(0) { properties.candlesImportFromDate }
        val toDate = java.time.LocalDate.now().toKotlinLocalDate().minus(DateTimeUnit.DAY)
        import(instrumentId, fromDate, toDate)
    }

    fun import(instrumentId: Int, dateFrom: LocalDate, dateTo: LocalDate) {
        import(
            instrumentId,
            dateFrom,
            ChronoUnit.DAYS.between(dateFrom.toJavaLocalDate(), dateTo.toJavaLocalDate()).toInt()
        )
    }

    fun import(instrumentId: Int, dateFrom: LocalDate, days: Int) {
        val figi = marketInstrumentClient.getFigi(instrumentId)
        import(instrumentId, figi, dateFrom, days)
    }

    private fun import(instrumentId: Int, instrumentFigi: String, dateFrom: LocalDate, days: Int) {
        log.info { "Import of dates range started, figi: $instrumentFigi, dateFrom: $dateFrom, days: $days" }

        (1..days).fold(dateFrom) { date, _ ->
            import(instrumentId, instrumentFigi, date)
            date.plus(DateTimeUnit.DAY)
        }

        log.info { "Import of dates range finished" }
    }

    private fun import(instrumentId: Int, instrumentFigi: String, date: LocalDate) {
        log.info { "Import of date started, figi: $instrumentFigi, date: $date" }

        val from = date.toJavaLocalDate().atStartOfDay(DATE_ZONE).toOffsetDateTime()
        val to = from.plusDays(1)
        val candles = openApiRateLimiter.execute {
            it.marketContext.getMarketCandles(instrumentFigi, from, to, CANDLE_RESOLUTION)
                .join()
                .map(Candles::getCandles)
                .orElseGet { listOf() }
        }
        log.info { "Requested candles: ${candles.size}" }

        save(candles, instrumentId)

        log.info { "Import of date finished" }

    }

    private fun save(
        candles: List<Candle>,
        instrumentId: Int
    ) {
        val candlesToPost = candles
            .map { mapper.map(it, instrumentId) }

        candleClient.post(candlesToPost)

        log.info { "Imported ${candles.size} candles, instrumentId: $instrumentId" }
    }

}