package com.larionov.stocks.tinkoffimporter

import kotlinx.datetime.LocalDate
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Configuration

@Configuration
@ConstructorBinding
@ConfigurationProperties(prefix = "tinkoff-importer")
data class TinkoffImporterProperties(

    val openApiToken: String = "t.iquR3KRi8He0iCt1D7Loj2pOxCfqFUUBzMaIw7Y47ws2V3uv9X6-mChH4d3_ON1msFAAS8QMNod49wx7QGVNsw",
    val sandbox: Boolean = true,
    val openApiPermitsPerSecond: Double = 6.0,

    val instrumentIdsForCandlesImport: List<Int> = listOf(5740, 7723),
    val candlesImportFromDate: LocalDate = LocalDate(2010, 1, 1)
)
