package com.larionov.stocks.tinkoffimporter

import com.larionov.stocks.services.market.api.CandleClient
import com.larionov.stocks.services.market.api.MarketInstrumentClient
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import ru.tinkoff.invest.openapi.OpenApi
import ru.tinkoff.invest.openapi.model.rest.SandboxRegisterRequest
import ru.tinkoff.invest.openapi.okhttp.OkHttpOpenApi

@SpringBootApplication
@EnableFeignClients(
    clients = [
        CandleClient::class,
        MarketInstrumentClient::class
    ]
)
@EnableScheduling
class TinkoffImporterApplication {

    @Bean
    fun okHttpOpenApi(props: TinkoffImporterProperties) = OkHttpOpenApi(props.openApiToken, props.sandbox)

    @Bean
    fun openApiSandBoxRegistrar(api: OpenApi) = InitializingBean {
        if (api.isSandboxMode) {
            // ОБЯЗАТЕЛЬНО нужно выполнить регистрацию в "песочнице"
            api.sandboxContext.performRegistration(SandboxRegisterRequest()).join()
        }
    }

}

fun main(args: Array<String>) {
    runApplication<TinkoffImporterApplication>(*args)
}
