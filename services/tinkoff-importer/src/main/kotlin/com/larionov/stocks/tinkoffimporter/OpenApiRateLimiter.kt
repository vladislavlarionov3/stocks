package com.larionov.stocks.tinkoffimporter

import com.google.common.util.concurrent.RateLimiter
import org.springframework.stereotype.Component
import ru.tinkoff.invest.openapi.OpenApi

@Component
class OpenApiRateLimiter(
        properties: TinkoffImporterProperties,
        private val openApi: OpenApi
) {
    @Suppress("UnstableApiUsage")
    private val limiter = RateLimiter.create(properties.openApiPermitsPerSecond)

    fun <T> execute(command: (api: OpenApi) -> T) = let {
        @Suppress("UnstableApiUsage")
        limiter.acquire(1)
    }.run {
        command.invoke(openApi)
    }

}