package com.larionov.stocks.tinkoffimporter.insrument

import com.larionov.stocks.services.market.api.MarketInstrumentClient
import com.larionov.stocks.tinkoffimporter.OpenApiRateLimiter
import mu.KotlinLogging
import org.springframework.stereotype.Component

@Component
class MarketInstrumentImporter(
    private val openApiRateLimiter: OpenApiRateLimiter,
    private val instrumentClient: MarketInstrumentClient,
    private val instrumentMapper: MarketInstrumentMapper
) {
    private val log = KotlinLogging.logger {}

    fun import() {
        log.info { "Import started" }

        val instruments = openApiRateLimiter.execute {
            it.marketContext.marketStocks.join().instruments
        }

        log.info { "${instruments.size} instruments requested from api" }

        val instrumentsToPost = instruments
            .filter { !instrumentClient.exists(it.figi) }
            .map { instrumentMapper.map(it) }
        log.info { "${instrumentsToPost.size} filtered for import" }

        instrumentClient.post(instrumentsToPost)
            .forEach { log.info { "Instrument imported: $it" } }

        log.info { "Import finished" }
    }

}