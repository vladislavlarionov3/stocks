package com.larionov.stocks.tinkoffimporter.insrument

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class InstrumentImportScheduler(
    private val instrumentImporter: MarketInstrumentImporter
) {
    @Scheduled(fixedDelay = 60 * 60 * 1000L)
    fun import() {
        instrumentImporter.import()
    }
}