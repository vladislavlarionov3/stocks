package com.larionov.stocks.tinkoffimporter.insrument

import com.larionov.stocks.services.market.api.MarketInstrumentDto
import org.springframework.stereotype.Component
import ru.tinkoff.invest.openapi.model.rest.MarketInstrument

@Component
class MarketInstrumentMapper {
    fun map(instrument: MarketInstrument) = MarketInstrumentDto(
        id = null,
        figi = instrument.figi,
        ticker = instrument.ticker,
        isin = instrument.isin,
        minPriceIncrement = instrument.minPriceIncrement?.toDouble(),
        lot = instrument.lot,
        minQuantity = instrument.minQuantity,
        currency = instrument.currency.name,
        name = instrument.name,
        type = instrument.type.name
    )
}