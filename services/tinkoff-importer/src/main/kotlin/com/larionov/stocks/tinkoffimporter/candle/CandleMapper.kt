package com.larionov.stocks.tinkoffimporter.candle

import com.larionov.stocks.services.market.api.CandleDto
import org.springframework.stereotype.Component
import ru.tinkoff.invest.openapi.model.rest.Candle

@Component
class CandleMapper {

    fun map(candle: Candle, instrumentId: Int) = CandleDto(
        id = null,
        instrumentId = instrumentId,
        instant = candle.time.toInstant(),
        open = candle.o.toDouble(),
        close = candle.c.toDouble(),
        high = candle.h.toDouble(),
        low = candle.l.toDouble(),
        volume = candle.v.toInt(),
        interval = candle.interval.name
    )

}