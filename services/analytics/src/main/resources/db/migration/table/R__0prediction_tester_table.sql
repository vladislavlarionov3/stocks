create foreign table if not exists function_value (
    id bigint not null,
    x double precision not null,
    y double precision not null
    )
    server prediction_tester
    options (table_name 'function_value');

create foreign table if not exists function_value_labels (
    function_value_id bigint not null,
    labels varchar(255)
    )
    server prediction_tester
    options (table_name 'function_value_labels');

create foreign table if not exists value_list_function (
    id bigint not null
    )
    server prediction_tester
    options (table_name 'value_list_function');

drop foreign table if exists prediction_test;
create foreign table if not exists prediction_test (
    id bigint not null,
    original_function_id bigint,
    creation_instant timestamp not null
    )
    server prediction_tester
    options (table_name 'prediction_test');

create foreign table if not exists prediction_test_result (
    creation_instant timestamp,
    test_id bigint not null
    )
    server prediction_tester
    options (table_name 'prediction_test_result');

create foreign table if not exists prediction_test_result_predicted_functions (
    prediction_test_result_test_id bigint not null,
    predicted_functions_id bigint not null
    )
    server prediction_tester
    options (table_name 'prediction_test_result_predicted_functions');

create foreign table if not exists value_list_function_labels (
    value_list_function_id bigint not null,
    labels varchar(255)
    )
    server prediction_tester
    options (table_name 'value_list_function_labels');

create foreign table if not exists value_list_function_values (
    value_list_function_id bigint not null,
    values_id bigint not null
    )
    server prediction_tester
    options (table_name 'value_list_function_values');