-- drop foreign table if exists market_instrument;
create foreign table if not exists market_instrument (
    id integer ,
    currency varchar(255),
    figi varchar(255),
    isin varchar(255),
    lot integer not null,
    min_price_increment double precision,
    min_quantity integer,
    name varchar(255),
    ticker varchar(255),
    type varchar(255)
    )
    server market_service
    options (table_name 'market_instrument');

-- drop foreign table if exists candle;
create foreign table if not exists candle (
    id bigint,
    close double precision not null,
    high double precision not null,
    instant timestamp,
    interval varchar(255),
    low double precision not null,
    open double precision not null,
    volume integer not null,
    instrument_id integer
    )
    server market_service
    options (table_name 'candle');


-- drop foreign table if exists test_result;
create foreign table if not exists test_result (
    id bigint,
    instant timestamp,
    starting_balance double precision not null
    )
    server trading_strategy_tester
    options (table_name 'test_result');

-- drop foreign table if exists strategy_test_result;
create foreign table if not exists strategy_test_result (
    id bigint,
    annual_rate double precision not null,
    buy_operations_number integer not null,
    final_balance double precision not null,
    sell_operations_number integer not null,
    strategy_name varchar(255),
    test_result_id bigint
    )
    server trading_strategy_tester
    options (table_name 'strategy_test_result');