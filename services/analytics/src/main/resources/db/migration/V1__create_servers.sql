create extension if not exists postgres_fdw;

create server if not exists market_service
    foreign data wrapper postgres_fdw
    options (dbname 'market-service');
create user mapping if not exists for postgres
    server market_service
    options (user 'postgres', password 'postgres');

create server if not exists trading_strategy_tester
    foreign data wrapper postgres_fdw
    options (dbname 'trading-strategy-tester');
create user mapping if not exists for postgres
    server trading_strategy_tester
    options (user 'postgres', password 'postgres');

create server if not exists prediction_tester
    foreign data wrapper postgres_fdw
    options (dbname 'prediction-tester');
create user mapping if not exists for postgres
    server prediction_tester
    options (user 'postgres', password 'postgres');