create or replace view last_predictors_test_id as
select id
from prediction_test
order by creation_instant desc
limit 1;

drop materialized view if exists last_predictors_test_chart;
drop materialized view if exists function_value_view;
drop materialized view if exists value_list_function_values_view;
drop materialized view if exists prediction_test_view;
drop materialized view if exists function_value_labels_view;

create materialized view function_value_view as
select *
from function_value;
create materialized view value_list_function_values_view as
select *
from value_list_function_values;
create materialized view prediction_test_view as
select *
from prediction_test;
create materialized view function_value_labels_view as
select *
from function_value_labels;

create index on value_list_function_values_view (value_list_function_id);
create index on function_value_view (id);
create index on function_value_labels_view (function_value_id);

drop materialized view if exists last_predictors_test_chart;
create materialized view last_predictors_test_chart as
with last_predicted_function as (
    select ptrpf.predicted_functions_id as id
    from prediction_test_result_predicted_functions ptrpf
    where ptrpf.prediction_test_result_test_id in (select * from last_predictors_test_id)
    union all
    select pt.original_function_id
    from prediction_test_view pt
    where pt.id in (select * from last_predictors_test_id)
)
select (timestamp 'epoch' + (
                                (1.64583264E9 - 1.51671162E9) * fv.x / 100 + 1.51671162E9
                                ) * interval '1 second') as "time",
       fvl.labels                                        as "metric",
       fv.y,
       fv.id
from value_list_function_values_view vlfv
         join function_value_view fv on vlfv.values_id = fv.id
         join function_value_labels_view fvl on fv.id = fvl.function_value_id
where vlfv.value_list_function_id in (select * from last_predicted_function);

create index on last_predictors_test_chart (time);

-- refresh materialized view last_predictors_test_chart;
-- refresh materialized view function_value_view;
-- refresh materialized view value_list_function_values_view;
-- refresh materialized view prediction_test_view;
-- refresh materialized view function_value_labels_view;
-- refresh materialized view last_predictors_test_chart;