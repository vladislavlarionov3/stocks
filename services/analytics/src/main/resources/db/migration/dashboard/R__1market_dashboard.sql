drop view if exists candle_chart;
create view candle_chart as
select c.instant as "time",
       mi.name   as "metric",
       c.close
from candle c
         join market_instrument mi on c.instrument_id = mi.id
where mi.id = 5740;
-- where mi.id = 7723;