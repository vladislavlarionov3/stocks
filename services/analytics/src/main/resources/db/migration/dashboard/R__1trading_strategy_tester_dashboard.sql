create or replace view last_test_result_id as
select id
from test_result
order by instant desc
limit 1;

drop view if exists last_test_table;
create view last_test_table as
select strategy_name,
       starting_balance,
       final_balance,
       annual_rate,
       buy_operations_number,
       sell_operations_number
from test_result tr
         join strategy_test_result str on tr.id = str.test_result_id
where tr.id in (select * from last_test_result_id)
order by final_balance desc;

drop view if exists last_test_bar_chart;
create view last_test_bar_chart as
select strategy_name, 100 * annual_rate as annual_rate
from test_result tr
         join strategy_test_result str on tr.id = str.test_result_id
where tr.id in (select * from last_test_result_id)
  and annual_rate < 100
order by final_balance;